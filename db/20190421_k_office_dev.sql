/*
SQLyog Professional v13.1.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - k_office_dev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`k_office_dev` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `k_office_dev`;

/*Table structure for table `contact_info` */

DROP TABLE IF EXISTS `contact_info`;

CREATE TABLE `contact_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('subject','about') COLLATE utf8_unicode_ci DEFAULT 'subject',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contact_info` */

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` bigint(20) unsigned NOT NULL,
  `subject_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `subject_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `subject_desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_id` bigint(20) unsigned NOT NULL,
  `about_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `about_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `about_desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `user_id` bigint(20) unsigned NOT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('NEW','CLOSED') COLLATE utf8_unicode_ci DEFAULT 'NEW',
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contacts_userId_users_id` (`user_id`),
  KEY `fk_contacts_subjectId_contactInfo_id` (`subject_id`),
  KEY `fk_contacts_aboutId_contactInfo_id` (`about_id`),
  CONSTRAINT `fk_contacts_aboutId_contactInfo_id` FOREIGN KEY (`about_id`) REFERENCES `contact_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_subjectId_contactInfo_id` FOREIGN KEY (`subject_id`) REFERENCES `contact_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_userId_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contacts` */

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments` */

insert  into `departments`(`id`,`code`,`name`,`description`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'code1','quan','23',1,1,'2019-04-02 10:50:49','USER: #6','2019-04-04 21:57:08','USER: #6','2019-04-04 21:57:08','USER: #6'),
(2,'Code','Name','Description',1,0,'2019-04-03 09:25:10','USER: #6','2019-04-03 09:25:23','USER: #6',NULL,NULL),
(3,'QT','Quản trị','Quản trị',1,0,'2019-04-04 21:56:59','USER: #6','2019-04-04 21:56:59','USER: #6',NULL,NULL),
(4,'code1','TEST',NULL,1,0,'2019-04-17 21:15:55','USER: #7','2019-04-17 21:15:55','USER: #7',NULL,NULL),
(6,'code2','TEST',NULL,1,0,'2019-04-17 21:16:01','USER: #7','2019-04-17 21:16:01','USER: #7',NULL,NULL),
(7,'code4','TEST',NULL,1,0,'2019-04-17 21:16:08','USER: #7','2019-04-17 21:16:08','USER: #7',NULL,NULL);

/*Table structure for table `exchanges` */

DROP TABLE IF EXISTS `exchanges`;

CREATE TABLE `exchanges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` bigint(20) unsigned DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_exchanges_tasks` (`issue_id`),
  CONSTRAINT `FK_exchanges_tasks` FOREIGN KEY (`issue_id`) REFERENCES `issues` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `exchanges` */

/*Table structure for table `issue_groups` */

DROP TABLE IF EXISTS `issue_groups`;

CREATE TABLE `issue_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `issue_groups` */

insert  into `issue_groups`(`id`,`code`,`name`,`description`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(3,'Test1','TEST','343',1,0,'2019-04-09 22:32:17','USER: #7','2019-04-09 22:35:59','USER: #7',NULL,NULL),
(4,'Test14','TEST','343',1,0,'2019-04-09 22:36:31','USER: #7','2019-04-09 22:36:31','USER: #7',NULL,NULL);

/*Table structure for table `issues` */

DROP TABLE IF EXISTS `issues`;

CREATE TABLE `issues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('manure','pesticide') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'manure',
  `status_issues` enum('manure','pesticide') COLLATE utf8mb4_unicode_ci DEFAULT 'manure',
  `group_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `priority` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `severity` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_user_id` bigint(20) unsigned DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tasks_users` (`user_id`),
  KEY `FK_tasks_category_tasks` (`group_id`),
  CONSTRAINT `FK_tasks_category_tasks` FOREIGN KEY (`group_id`) REFERENCES `issue_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tasks_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `issues` */

insert  into `issues`(`id`,`name`,`code`,`description`,`status`,`status_issues`,`group_id`,`user_id`,`priority`,`parent_id`,`severity`,`assign_user_id`,`deadline`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'Sang1','Tassk1',NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'2019-04-21 15:34:54','USER: #8','2019-04-21 15:34:54','USER: #8',NULL,NULL),
(2,'Sang1','Tassk12',NULL,NULL,'manure',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'2019-04-21 16:26:53','USER: #8','2019-04-21 16:26:53','USER: #8',NULL,NULL);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `thumbnail` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `view_count` int(10) unsigned DEFAULT NULL,
  `like_count` int(10) unsigned DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  `is_approved` tinyint(3) unsigned DEFAULT '1',
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news` */

/*Table structure for table `permission_groups` */

DROP TABLE IF EXISTS `permission_groups`;

CREATE TABLE `permission_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_groups` */

insert  into `permission_groups`(`id`,`name`,`code`,`description`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(4,'User','USER','User Group',1,0,'2018-09-28 20:55:18','1','2018-09-28 20:55:20','1',NULL,NULL),
(8,'Support','SUPPORT','Support',1,0,'2018-09-29 23:32:57','1',NULL,NULL,NULL,NULL),
(25,'Permission Group','PERMISSION-GROUP','Permission group',1,0,'2019-01-11 10:08:50','1',NULL,NULL,NULL,NULL),
(35,'Role','Role','Role',1,0,'2019-03-18 16:18:28','1',NULL,NULL,NULL,NULL),
(38,'DEPARTMENTS','DEPARTMENTS','DEPARTMENTS',1,0,'2019-04-02 21:13:17',NULL,NULL,NULL,NULL,NULL),
(39,'Profile','Profile',NULL,1,0,'2019-04-02 21:35:44',NULL,NULL,NULL,NULL,NULL),
(40,'Issue','Issue','Issue\r\n',1,0,'2019-04-08 10:53:35','1',NULL,NULL,NULL,NULL),
(41,'Group Issue','Group Issue','Group Issue',1,0,'2019-04-08 11:47:23','1',NULL,NULL,NULL,NULL);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `group_id` bigint(20) unsigned DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_permissions_groupId_permissionGroups_id` (`group_id`),
  CONSTRAINT `FK_permissions_groupId_permissionGroups_id` FOREIGN KEY (`group_id`) REFERENCES `permission_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`code`,`description`,`group_id`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(4,'VIEW-USER1','VIEW-USER','Can view User',4,1,0,'2018-09-28 20:52:50','1','2018-09-28 20:52:53','1',NULL,NULL),
(5,'UPDATE-USER','UPDATE-USER','Can update, create User',4,1,0,'2018-09-28 20:56:56','1','2018-09-28 20:56:58','1',NULL,NULL),
(6,'DELETE-USER','DELETE-USER','Can delete user',4,1,0,'2018-09-28 20:57:38','1','2018-09-28 20:57:39','1',NULL,NULL),
(11,'VIEW-SUPPORT','VIEW-SUPPORT','Can view support',8,1,0,'2018-09-29 23:34:27','1',NULL,NULL,NULL,NULL),
(12,'CREATE-SUPPORT','CREATE-SUPPORT','Can create support',8,1,0,'2018-09-29 23:35:07','1',NULL,NULL,NULL,NULL),
(13,'UPDATE-SUPPORT','UPDATE-SUPPORT','Can update support',8,1,0,'2018-09-29 23:35:57','1',NULL,NULL,NULL,NULL),
(25,'DELETE-SUPPORT','DELETE-SUPPORT','Can delete support',8,1,0,'2018-10-14 14:03:02','1',NULL,NULL,NULL,NULL),
(83,'VIEW-PERMISSION-GROUP','VIEW-PERMISSION-GROUP','Admin can view permission group',25,1,0,'2019-01-11 10:10:06','1',NULL,NULL,NULL,NULL),
(84,'CREATE-PERMISSION-GROUP','CREATE-PERMISSION-GROUP','Admin can create permission group',25,1,0,'2019-01-11 10:10:57','1',NULL,NULL,NULL,NULL),
(85,'UPDATE-PERMISSION-GROUP','UPDATE-PERMISSION-GROUP','Admin can update permission group',25,1,0,'2019-01-11 10:12:08','1',NULL,NULL,NULL,NULL),
(86,'DELETE-PERMISSION-GROUP','DELETE-PERMISSION-GROUP','Admin can delete permission group',25,1,0,'2019-01-11 10:12:48','1',NULL,NULL,NULL,NULL),
(122,'VIEW-ROLE','VIEW-ROLE','Admin VIEW-ROLE',35,1,0,'2019-03-18 16:21:08','1',NULL,NULL,NULL,NULL),
(123,'CREATE-ROLE','CREATE-ROLE','Admin CREATE-ROLE',35,1,0,'2019-03-18 16:22:00','1',NULL,NULL,NULL,NULL),
(124,'UPDATE-ROLE','UPDATE-ROLE','Admin',35,1,0,'2019-03-18 16:22:41','1',NULL,NULL,NULL,NULL),
(125,'DELETE-ROLE','DELETE-ROLE','Admin DELETE-ROLE',35,1,0,'2019-03-18 16:23:16','1',NULL,NULL,NULL,NULL),
(126,'UPDATE-ROLE-PERMISSIONS','UPDATE-ROLE-PERMISSIONS','Admin UPDATE-ROLE-PERMISSIONS',35,1,0,'2019-03-18 16:23:54','1',NULL,NULL,NULL,NULL),
(130,'VIEW-DEPARTMENTS','VIEW-DEPARTMENTS','Admin Can VIEW-DEPARTMENTS',38,1,0,'2019-04-02 21:14:26',NULL,NULL,NULL,NULL,NULL),
(131,'CREATE-DEPARTMENTS','CREATE-DEPARTMENTS','Admin Can CREATE-DEPARTMENTS',38,1,0,'2019-04-02 21:14:51',NULL,NULL,NULL,NULL,NULL),
(132,'UPDATE-DEPARTMENTS','UPDATE-DEPARTMENTS','UPDATE-DEPARTMENTS',38,1,0,'2019-04-02 21:15:15',NULL,NULL,NULL,NULL,NULL),
(133,'DELETE-DEPARTMENTS','DELETE-DEPARTMENTS','Admin Can DELETE-DEPARTMENTS',38,1,0,'2019-04-02 21:15:42',NULL,NULL,NULL,NULL,NULL),
(134,'VIEW-PROFILE','VIEW-PROFILE','Admin Can VIEW-PROFILE',39,1,0,'2019-04-02 21:36:29',NULL,NULL,NULL,NULL,NULL),
(135,'VIEW-ISSUE','VIEW-ISSUE','Admin Can VIEW-ISSUE',40,1,0,'2019-04-08 10:54:17','1',NULL,NULL,NULL,NULL),
(136,'UPDATE-ISSUE','UPDATE-ISSUE','Admin Can UPDATE-ISSUE',40,1,0,'2019-04-08 10:54:59','1',NULL,NULL,NULL,NULL),
(137,'CREATE-ISSUE','CREATE-ISSUE','Admin Can CREATE-ISSUE',40,1,0,'2019-04-08 10:56:00','1',NULL,NULL,NULL,NULL),
(138,'DELETE-ISSUE','DELETE-ISSUE','Admin Can DELETE-ISSUE',40,1,0,'2019-04-08 10:56:31','1',NULL,NULL,NULL,NULL),
(139,'VIEW-GROUP-ISSUE','VIEW-GROUP-ISSUE','Admin Can VIEW-GROUP-ISSUE',41,1,0,'2019-04-08 11:48:18','1',NULL,NULL,NULL,NULL),
(141,'CREATE-GROUP-ISSUE','CREATE-GROUP-ISSUE','Admin Can CREATE-GROUP-ISSUE',40,1,0,'2019-04-08 11:48:59',NULL,NULL,NULL,NULL,NULL),
(142,'UPDATE-GROUP-ISSUE','UPDATE-GROUP-ISSUE','Admin Can UPDATE-GROUP-ISSUE',40,1,0,'2019-04-08 11:49:25',NULL,NULL,NULL,NULL,NULL),
(143,'DELETE-GROUP-ISSUE','DELETE-GROUP-ISSUE','Admin Can DELETE-GROUP-ISSUE',40,1,0,'2019-04-08 11:49:47',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `profiles` */

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `genre` enum('M','F','O') COLLATE utf8_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci,
  `id_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_deleted_at` (`email`,`deleted_at`),
  KEY `FK_profiles_userId_users_id` (`user_id`),
  CONSTRAINT `FK_profiles_userId_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `profiles` */

insert  into `profiles`(`id`,`user_id`,`email`,`first_name`,`last_name`,`short_name`,`full_name`,`address`,`phone`,`birthday`,`genre`,`avatar`,`id_number`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(5,5,'ryanthanhtien@gmail.com','TETS1234',NULL,'TETS1234','TETS1234',NULL,'0367560394',NULL,'O',NULL,'0',1,0,'2019-01-04 10:33:58','USER: #7','2019-04-10 09:22:22','USER: #7',NULL,NULL),
(6,6,'huequan98@gmail.com','Phạm','Văn Quân','Phạm Văn Quân','Phạm Văn Quân',NULL,'123456789',NULL,'O',NULL,'0',1,0,'2019-01-05 13:54:43','USER: #6','2019-04-08 09:54:03','USER: #6',NULL,NULL),
(7,7,'trantiendat041198@gmail.com','Tiến','Đạt','Tiến Đạt','Tiến Đạt',NULL,'123544213',NULL,'O',NULL,'0',1,0,'2019-01-05 13:55:33','USER: #1','2019-02-20 09:14:29','USER: #1',NULL,NULL),
(8,8,'sangnguyen0909ds@gmail.com','Nguyễn','Văn Sang','Nguyễn Văn Sang','Nguyễn Văn Sang',NULL,'12381274',NULL,'O',NULL,'0',1,0,'2019-01-06 10:15:48','USER: #6','2019-03-21 12:02:26','USER: #6',NULL,NULL),
(9,9,'hieu.phan@kpis.com.vn','SystemAdmin2',NULL,'SystemAdmin2','SystemAdmin2',NULL,'0966944164',NULL,'O',NULL,'0',1,0,'2019-01-06 10:16:23','USER: #6','2019-03-22 12:38:49','USER: #6',NULL,NULL),
(60,2,'tien@kpis.com.vn','Nguyễn','Thanh Tiến','Nguyễn Thanh Tiến','Nguyễn Thanh Tiến',NULL,'969033276',NULL,'O',NULL,'0',1,0,'2019-03-16 21:36:48','USER: #6','2019-04-08 10:20:11','USER: #6',NULL,NULL),
(62,61,'ryanah@gmail.com','Cẩm','cuốn','Cẩm cuốn','Cẩm cuốn',NULL,'9999999',NULL,'O',NULL,'0',1,0,'2019-03-20 19:38:02','USER: #2','2019-03-20 19:38:02','USER: #2',NULL,NULL),
(64,63,'sang.nguyen09ds@gmail.com','Nguyễn','Văn Sang','Nguyễn Văn Sang','Nguyễn Văn Sang',NULL,'394679933',NULL,'O',NULL,'0',1,0,'2019-03-21 14:58:27','USER: #57','2019-03-21 14:58:27','USER: #57',NULL,NULL),
(65,64,'vanquan@gmail.com','Quanvan',NULL,'Quanvan','Quanvan',NULL,'9090909',NULL,'O',NULL,'0',1,0,'2019-03-22 11:22:26','USER: #6','2019-03-22 11:22:26','USER: #6',NULL,NULL),
(69,68,'sang1234@gmail.com','sang1234',NULL,'sang1234','sang1234',NULL,'23456',NULL,'O',NULL,'0',1,0,'2019-03-31 15:39:02','USER: #52','2019-03-31 15:41:29','USER: #52',NULL,NULL),
(70,69,'quan98@gmail.com','name98',NULL,'name98','name98',NULL,'969033276',NULL,'O',NULL,'0',1,0,'2019-04-04 21:03:38','USER: #6','2019-04-04 21:03:38','USER: #6',NULL,NULL),
(71,70,'q@gmail.com','username',NULL,'username','username',NULL,'969033276',NULL,'O',NULL,'0',1,0,'2019-04-08 10:06:13','USER: #6','2019-04-08 10:18:07','USER: #6',NULL,NULL),
(72,71,'s@gmail.com','UserName',NULL,'UserName','UserName',NULL,'969033276',NULL,'O',NULL,'0',1,0,'2019-04-08 10:19:17','USER: #6','2019-04-08 10:19:17','USER: #6',NULL,NULL);

/*Table structure for table `role_permissions` */

DROP TABLE IF EXISTS `role_permissions`;

CREATE TABLE `role_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id_permission_id_deleted_at` (`role_id`,`permission_id`,`deleted_at`),
  KEY `FK_rolePermissions_permissionId_permissions_id` (`permission_id`),
  CONSTRAINT `FK_rolePermissions_permissionId_permissions_id` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_rolePermissions_roleId_roles_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_permissions` */

insert  into `role_permissions`(`id`,`role_id`,`permission_id`,`description`,`is_active`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(193,1,4,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(195,1,5,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(196,1,6,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(197,1,11,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(198,1,12,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(199,1,13,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(200,1,25,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(201,1,83,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(202,1,84,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(203,1,85,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(204,1,86,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(205,1,122,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(206,1,123,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(207,1,124,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(208,1,125,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(210,1,126,NULL,1,0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),
(211,1,130,NULL,1,0,'2019-04-02 21:16:04',NULL,NULL,NULL,NULL,NULL),
(212,1,131,NULL,1,0,'2019-04-02 21:16:15',NULL,NULL,NULL,NULL,NULL),
(213,1,132,NULL,1,0,'2019-04-02 21:16:25',NULL,NULL,NULL,NULL,NULL),
(214,1,133,NULL,1,0,'2019-04-02 21:16:37',NULL,NULL,NULL,NULL,NULL),
(215,1,134,NULL,1,0,'2019-04-02 21:36:50',NULL,NULL,NULL,NULL,NULL),
(216,1,135,NULL,1,0,'2019-04-08 11:00:50',NULL,NULL,NULL,NULL,NULL),
(217,1,136,NULL,1,0,'2019-04-08 11:00:58',NULL,NULL,NULL,NULL,NULL),
(218,1,137,NULL,1,0,'2019-04-08 11:01:09',NULL,NULL,NULL,NULL,NULL),
(219,1,138,NULL,1,0,'2019-04-08 11:01:16',NULL,NULL,NULL,NULL,NULL),
(220,1,139,NULL,1,0,'2019-04-08 11:50:31',NULL,NULL,NULL,NULL,NULL),
(222,1,141,NULL,1,0,'2019-04-08 11:50:41',NULL,NULL,NULL,NULL,NULL),
(223,1,142,NULL,1,0,'2019-04-08 11:51:42',NULL,NULL,NULL,NULL,NULL),
(224,1,143,NULL,1,0,'2019-04-08 11:51:48',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `role_level` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_deleted_at` (`code`,`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`code`,`name`,`description`,`is_active`,`role_level`,`status`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'ADMIN','Admin',NULL,1,1,1,0,'2017-04-15 09:49:56','USER: #2','2019-03-26 11:29:24','USER: #2',NULL,NULL),
(2,'Manager','Manager',NULL,1,2,1,0,'2017-04-16 07:42:58','USER: #2','2019-03-26 11:29:26','USER: #2',NULL,NULL),
(3,'Employee','Employee',NULL,0,3,1,0,'2017-04-20 16:28:08','USER: #2','2019-03-26 11:29:30','USER: #2',NULL,NULL),
(4,'GUEST','Guest',NULL,1,4,1,0,'2018-10-11 19:05:11','USER: #2','2019-03-26 11:29:34','USER: #2',NULL,NULL),
(5,'ABC','ABC',NULL,0,NULL,0,1,'2019-03-19 16:47:29','USER: #4','2019-03-20 09:47:55','USER: #4','2019-03-20 09:47:55','USER: #4'),
(6,'Empl','Employee',NULL,1,5,1,0,'2019-03-20 08:58:45','USER: #2','2019-03-26 11:29:38','USER: #2',NULL,NULL),
(7,'Editer','Editer',NULL,1,6,1,0,'2019-03-20 10:06:30','USER: #2','2019-03-26 11:29:42','USER: #2',NULL,NULL),
(8,'aaaaa','aa',NULL,1,NULL,1,1,'2019-03-20 10:17:52','USER: #4','2019-03-20 10:18:25','USER: #4','2019-03-20 10:18:25','USER: #4'),
(9,'â','â',NULL,1,NULL,1,1,'2019-03-20 10:19:57','USER: #4','2019-03-20 10:20:01','USER: #4','2019-03-20 10:20:01','USER: #4'),
(10,'Role Code','Role Name',NULL,1,7,1,0,'2019-03-26 10:54:58','USER: #2','2019-03-26 11:29:46','USER: #2',NULL,NULL),
(11,'TEST','TESST',NULL,1,NULL,NULL,1,'2019-04-11 08:51:05','USER: #7','2019-04-11 08:51:34','USER: #7','2019-04-11 08:51:34','USER: #7'),
(13,'TEST1','TESST1',NULL,1,NULL,NULL,0,'2019-04-11 09:08:25','USER: #7','2019-04-11 09:08:25','USER: #7',NULL,NULL),
(15,'TEST1','TESST1',NULL,1,NULL,NULL,1,'2019-04-11 09:11:18','USER: #7','2019-04-11 09:11:30','USER: #7','2019-04-11 09:11:30','USER: #7'),
(16,'TEST1','TESST1',NULL,1,NULL,NULL,0,'2019-04-11 09:11:40','USER: #7','2019-04-11 09:11:40','USER: #7',NULL,NULL);

/*Table structure for table `supports` */

DROP TABLE IF EXISTS `supports`;

CREATE TABLE `supports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('manure','pesticide') COLLATE utf8_unicode_ci DEFAULT 'manure',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `supports` */

/*Table structure for table `user_logs` */

DROP TABLE IF EXISTS `user_logs`;

CREATE TABLE `user_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(50) NOT NULL,
  `target` varchar(255) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `browser` text,
  `user_id` bigint(20) DEFAULT NULL,
  `old_data` text,
  `new_data` text,
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_logs` */

/*Table structure for table `user_sessions` */

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `token` varchar(400) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_at` datetime NOT NULL,
  `expired_at` datetime NOT NULL,
  `device_type` enum('DESKTOP','TABLET','PHONE','ANDROID','IOS','UNKNOWN') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'UNKNOWN',
  `device_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_sessions_users` (`user_id`),
  CONSTRAINT `FK_user_sessions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_sessions` */

insert  into `user_sessions`(`id`,`user_id`,`token`,`login_at`,`expired_at`,`device_type`,`device_id`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(5,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNDM1NiwiZXhwIjoxNTg1NjQwMzU2LCJuYmYiOjE1NTQxMDQzNTYsImp0aSI6ImxMaExlVU9OdktibUpRaFMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Cy4TeVFQ-k3flen7UPwPzobNr9LYf96ztTGpFsFBh7I','2019-04-01 14:39:16','2020-03-31 14:39:16','UNKNOWN',NULL,1,'2019-04-01 14:39:16','6','2019-04-01 14:42:00',NULL,'2019-04-01 14:42:00',NULL),
(6,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNDUyMCwiZXhwIjoxNTg1NjQwNTIwLCJuYmYiOjE1NTQxMDQ1MjAsImp0aSI6IllyaGNjVlVRdkd2VTRPQTEiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.TDynyFqber2fKGS8QLtAfLuXGX9fEgYOy05Hgp7zSRU','2019-04-01 14:42:00','2020-03-31 14:42:00','UNKNOWN',NULL,1,'2019-04-01 14:42:00','6','2019-04-01 14:42:50',NULL,'2019-04-01 14:42:50',NULL),
(7,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNDU3MCwiZXhwIjoxNTg1NjQwNTcwLCJuYmYiOjE1NTQxMDQ1NzAsImp0aSI6IktWTktMMXg2SWd4Q21EdzMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.TALquHgMsFH4TVyzmLIV2j9dZPBKGncb_kHDIRH0McU','2019-04-01 14:42:50','2020-03-31 14:42:50','UNKNOWN',NULL,1,'2019-04-01 14:42:50','6','2019-04-01 14:44:36',NULL,'2019-04-01 14:44:36',NULL),
(8,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNDY3NiwiZXhwIjoxNTg1NjQwNjc2LCJuYmYiOjE1NTQxMDQ2NzYsImp0aSI6IjFMSjliU3M3czZXWFoyQ0kiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.lDA2RsMPm20lH_kP_qyRBBp3TjjnY2F8YT1VH3tNJ20','2019-04-01 14:44:36','2020-03-31 14:44:36','UNKNOWN',NULL,1,'2019-04-01 14:44:36','6','2019-04-01 14:47:45',NULL,'2019-04-01 14:47:45',NULL),
(9,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNDg2NSwiZXhwIjoxNTg1NjQwODY1LCJuYmYiOjE1NTQxMDQ4NjUsImp0aSI6Im43OWg0MW5UZlFZU01VMzgiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.ospyAmnxm-L-FumM6aGZkPPCFFCA76scgbeoR_zfv34','2019-04-01 14:47:45','2020-03-31 14:47:45','UNKNOWN',NULL,1,'2019-04-01 14:47:45','6','2019-04-01 14:50:44',NULL,'2019-04-01 14:50:44',NULL),
(10,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEwNTA0NCwiZXhwIjoxNTg1NjQxMDQ0LCJuYmYiOjE1NTQxMDUwNDQsImp0aSI6IjRRYVFCdG5qaFpwRWhaczMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Clo4fQP0ltFaLjtrUcV0ofrt399kjLOosZXlfFGyeu4','2019-04-01 14:50:44','2020-03-31 14:50:44','UNKNOWN',NULL,1,'2019-04-01 14:50:44','6','2019-04-01 16:39:09',NULL,'2019-04-01 16:39:09',NULL),
(11,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDExMTU0OSwiZXhwIjoxNTg1NjQ3NTQ5LCJuYmYiOjE1NTQxMTE1NDksImp0aSI6IkVzSlpXVWhrRkh5b1ZhRk8iLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DgYqzo2ioHj4CQ92N1zbwKD-iVlqIVfc00dynR7Pni0','2019-04-01 16:39:09','2020-03-31 16:39:09','UNKNOWN',NULL,1,'2019-04-01 16:39:09','6','2019-04-01 16:50:23',NULL,'2019-04-01 16:50:23',NULL),
(12,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDExMjIyMywiZXhwIjoxNTg1NjQ4MjIzLCJuYmYiOjE1NTQxMTIyMjMsImp0aSI6IllwWTBVUnBYSkNtVkk2Z2IiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.VpWw82kIkQVW8P9ta9rWBYv-t1EVdcWbXrv6xfaIlRU','2019-04-01 16:50:23','2020-03-31 16:50:23','UNKNOWN',NULL,1,'2019-04-01 16:50:23','6','2019-04-01 16:58:43',NULL,'2019-04-01 16:58:43',NULL),
(13,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDExMjcyMywiZXhwIjoxNTg1NjQ4NzIzLCJuYmYiOjE1NTQxMTI3MjMsImp0aSI6IlhzcnU1Y21sRU00UVVKeDQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.UosGcoLtZ4_20rEGVS1SHavX87aL_wOUMduj3HXNcUU','2019-04-01 16:58:43','2020-03-31 16:58:43','UNKNOWN',NULL,1,'2019-04-01 16:58:43','6','2019-04-01 17:02:11',NULL,'2019-04-01 17:02:11',NULL),
(14,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDExMjkzMSwiZXhwIjoxNTg1NjQ4OTMxLCJuYmYiOjE1NTQxMTI5MzEsImp0aSI6IkxneTQzeG9wQ0tIaHBIWXciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.LKWadYO06NpHaAg6BytHIOlHg_71rF1481Isjna7WiU','2019-04-01 17:02:11','2020-03-31 17:02:11','UNKNOWN',NULL,1,'2019-04-01 17:02:11','6','2019-04-01 17:18:03',NULL,'2019-04-01 17:18:03',NULL),
(15,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDExMzg4MywiZXhwIjoxNTg1NjQ5ODgzLCJuYmYiOjE1NTQxMTM4ODMsImp0aSI6IkxtU0JtQXBua3U0eXh4dFUiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.NRiNo-GgQiRPxbLamS2UGQISy311-G681TMPGhYxwng','2019-04-01 17:18:03','2020-03-31 17:18:03','UNKNOWN',NULL,1,'2019-04-01 17:18:03','6','2019-04-01 21:52:38',NULL,'2019-04-01 21:52:38',NULL),
(16,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEzMDM1OCwiZXhwIjoxNTg1NjY2MzU4LCJuYmYiOjE1NTQxMzAzNTgsImp0aSI6IlZTSmJ2eWVOMElxV0VQalEiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.tx1TwqOb2XV_YYED3XA9oL0rX0itwjyG5CIO_L6fDDg','2019-04-01 21:52:38','2020-03-31 21:52:38','UNKNOWN',NULL,1,'2019-04-01 21:52:38','6','2019-04-01 22:09:07',NULL,'2019-04-01 22:09:07',NULL),
(17,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDEzMTM0NywiZXhwIjoxNTg1NjY3MzQ3LCJuYmYiOjE1NTQxMzEzNDcsImp0aSI6Ik4zWElqaVcwNWg4bWRmV0IiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.jaJ4whwQ60ZZUy-ru9Wd5GRaBBYdUWdcuZhcmVgeyWo','2019-04-01 22:09:07','2020-03-31 22:09:07','UNKNOWN',NULL,1,'2019-04-01 22:09:07','6','2019-04-02 10:32:09',NULL,'2019-04-02 10:32:09',NULL),
(18,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDE3NTkyOSwiZXhwIjoxNTg1NzExOTI5LCJuYmYiOjE1NTQxNzU5MjksImp0aSI6ImNwbTU3MHBHNlVndHdobjAiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.66UKtt5u5jGIZczOq5cIl-n6rc7BodVFz85fm4q2hJ0','2019-04-02 10:32:09','2020-04-01 10:32:09','UNKNOWN',NULL,1,'2019-04-02 10:32:09','6','2019-04-02 14:52:23',NULL,'2019-04-02 14:52:23',NULL),
(19,7,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLWNtcy5rcGlzLnZuL2F1dGgvbG9naW4iLCJpYXQiOjE1NTQxOTAxNzEsImV4cCI6MTU4NTcyNjE3MSwibmJmIjoxNTU0MTkwMTcxLCJqdGkiOiJEd0p4WHJQSU5NMm5ac0p3Iiwic3ViIjo3LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.GSTPKqPV_64wfUFcUxF2hQt9Mgf-W22dLgLoDeyiIAw','2019-04-02 14:29:31','2020-04-01 14:29:31','UNKNOWN',NULL,1,'2019-04-02 14:29:31','7','2019-04-08 11:00:06',NULL,'2019-04-08 11:00:06',NULL),
(20,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDE5MTU0MywiZXhwIjoxNTg1NzI3NTQzLCJuYmYiOjE1NTQxOTE1NDMsImp0aSI6IkxjVm1vWEt5bm1NaEU1dEsiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.CQJ2KrgHWmipUpZP8F5f4XBxMT4i83wcEANcAONWdBA','2019-04-02 14:52:23','2020-04-01 14:52:23','UNKNOWN',NULL,1,'2019-04-02 14:52:23','6','2019-04-02 21:22:11',NULL,'2019-04-02 21:22:11',NULL),
(21,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNDkzMSwiZXhwIjoxNTg1NzUwOTMxLCJuYmYiOjE1NTQyMTQ5MzEsImp0aSI6IkdTa3ROakFsVkNOSUhQaGwiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.MNBAjVE58bUiLg5s6eMNnCCYu0x9_F-eBTCjsX7tIY8','2019-04-02 21:22:11','2020-04-01 21:22:11','UNKNOWN',NULL,1,'2019-04-02 21:22:11','6','2019-04-02 21:22:27',NULL,'2019-04-02 21:22:27',NULL),
(22,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNDk0NywiZXhwIjoxNTg1NzUwOTQ3LCJuYmYiOjE1NTQyMTQ5NDcsImp0aSI6IlcxVG5IN2Rnd0drS0pWVmIiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.xptbEgAK-wsXyOTDb3kSMLE51pd9fEtZa_XxhwnsDPY','2019-04-02 21:22:27','2020-04-01 21:22:27','UNKNOWN',NULL,1,'2019-04-02 21:22:27','6','2019-04-02 21:30:39',NULL,'2019-04-02 21:30:39',NULL),
(23,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNTQzOSwiZXhwIjoxNTg1NzUxNDM5LCJuYmYiOjE1NTQyMTU0MzksImp0aSI6Ing3YVhtcFVUY2JMWHRQOFkiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.L4UDthvZxG4Rt4UC95w6MNkA0B_LefEF3hKysmVBZ4k','2019-04-02 21:30:39','2020-04-01 21:30:39','UNKNOWN',NULL,1,'2019-04-02 21:30:39','6','2019-04-02 21:30:51',NULL,'2019-04-02 21:30:51',NULL),
(24,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNTQ1MSwiZXhwIjoxNTg1NzUxNDUxLCJuYmYiOjE1NTQyMTU0NTEsImp0aSI6IjJPVDNwcDdPeEVRdjljaGMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.QBOgpQxliUmruANYdFMVtMBmSx7dVIvwMS2NomJ6rO8','2019-04-02 21:30:51','2020-04-01 21:30:51','UNKNOWN',NULL,1,'2019-04-02 21:30:51','6','2019-04-02 21:31:41',NULL,'2019-04-02 21:31:41',NULL),
(25,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNTUwMSwiZXhwIjoxNTg1NzUxNTAxLCJuYmYiOjE1NTQyMTU1MDEsImp0aSI6IkZENG5EdldUSFFXd3U4Q3MiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.7oiXqx_9pj4yTC3ZW7aSBT3dJb25KAQn0BfndaaBgnc','2019-04-02 21:31:41','2020-04-01 21:31:41','UNKNOWN',NULL,1,'2019-04-02 21:31:41','6','2019-04-02 21:41:14',NULL,'2019-04-02 21:41:14',NULL),
(26,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDIxNjA3NCwiZXhwIjoxNTg1NzUyMDc0LCJuYmYiOjE1NTQyMTYwNzQsImp0aSI6Ilk4S2VxV0czTnJFb21wbjMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.RlH8aKhZT3m5F4eNn-5-IF4RFD84PhsLg3Yb-MtBDVY','2019-04-02 21:41:14','2020-04-01 21:41:14','UNKNOWN',NULL,1,'2019-04-02 21:41:14','6','2019-04-03 09:42:28',NULL,'2019-04-03 09:42:28',NULL),
(27,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI1OTM0OCwiZXhwIjoxNTg1Nzk1MzQ4LCJuYmYiOjE1NTQyNTkzNDgsImp0aSI6ImFOejFPRWlHTjdmOTZRS2MiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.ySTD0FBDiKjG3uFFEzvKvv5VxzzP7Z04hOnAxMFlOkg','2019-04-03 09:42:28','2020-04-02 09:42:28','UNKNOWN',NULL,1,'2019-04-03 09:42:28','6','2019-04-03 09:48:25',NULL,'2019-04-03 09:48:25',NULL),
(28,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI1OTcwNSwiZXhwIjoxNTg1Nzk1NzA1LCJuYmYiOjE1NTQyNTk3MDUsImp0aSI6Ikp4NVRFd3lWOVc1Y1pYNGUiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Dn9fOn0ersv8YMNTPr-bT38XzG9cY07RZaAIW6FXsEY','2019-04-03 09:48:25','2020-04-02 09:48:25','UNKNOWN',NULL,1,'2019-04-03 09:48:25','6','2019-04-03 09:51:14',NULL,'2019-04-03 09:51:14',NULL),
(29,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI1OTg3NCwiZXhwIjoxNTg1Nzk1ODc0LCJuYmYiOjE1NTQyNTk4NzQsImp0aSI6IlhZTzc5VEt1VzF4TFBKVjQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.V8vhGuQ-f9Kfj009CZx8P5dycNwtLxQ74tv0McYhXw8','2019-04-03 09:51:14','2020-04-02 09:51:14','UNKNOWN',NULL,1,'2019-04-03 09:51:14','6','2019-04-03 09:52:03',NULL,'2019-04-03 09:52:03',NULL),
(30,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI1OTkyMywiZXhwIjoxNTg1Nzk1OTIzLCJuYmYiOjE1NTQyNTk5MjMsImp0aSI6Im1mTGZ5a2Z0UEVVaFB1VlkiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.2hm0rG_jSkYtzevCTHtIBdbQ4Vh5FErc_EuuVixDXm4','2019-04-03 09:52:03','2020-04-02 09:52:03','UNKNOWN',NULL,1,'2019-04-03 09:52:03','6','2019-04-03 10:07:34',NULL,'2019-04-03 10:07:34',NULL),
(31,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI2MDg1NCwiZXhwIjoxNTg1Nzk2ODU0LCJuYmYiOjE1NTQyNjA4NTQsImp0aSI6Im5ESkJOcWlTeklHczk2blciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.nLx8KJgnNUCRu3ixt0N8RWaL9tZ0vTXZsiRBSi1h7WU','2019-04-03 10:07:34','2020-04-02 10:07:34','UNKNOWN',NULL,1,'2019-04-03 10:07:34','6','2019-04-03 10:09:45',NULL,'2019-04-03 10:09:45',NULL),
(32,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI2MDk4NSwiZXhwIjoxNTg1Nzk2OTg1LCJuYmYiOjE1NTQyNjA5ODUsImp0aSI6IndsZDBXZFFrdVVFR2JTN3giLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.MCN3gdjU4cyggU-gb8UYxc2bvdsAc5AT_5CM7dZAoPw','2019-04-03 10:09:45','2020-04-02 10:09:45','UNKNOWN',NULL,1,'2019-04-03 10:09:45','6','2019-04-03 10:23:57',NULL,'2019-04-03 10:23:57',NULL),
(33,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDI2MTgzNywiZXhwIjoxNTg1Nzk3ODM3LCJuYmYiOjE1NTQyNjE4MzcsImp0aSI6Ik80MHdmbnpQYnZ5MzRXYzkiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.jjkN-hBLkwuTzWhsCWaCO9gUE_0pzNUUjdZlzUCJKDw','2019-04-03 10:23:57','2020-04-02 10:23:57','UNKNOWN',NULL,1,'2019-04-03 10:23:57','6','2019-04-03 21:05:56',NULL,'2019-04-03 21:05:56',NULL),
(34,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDMwMDM1NiwiZXhwIjoxNTg1ODM2MzU2LCJuYmYiOjE1NTQzMDAzNTYsImp0aSI6IlBCMTZtZVk4TXNmZ1IyTUwiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.qblK12_ZgatzJFRoxGYxdLoGAtjfKOaEpg-AZ_PCg6A','2019-04-03 21:05:56','2020-04-02 21:05:56','UNKNOWN',NULL,1,'2019-04-03 21:05:56','6','2019-04-03 21:10:56',NULL,'2019-04-03 21:10:56',NULL),
(35,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDMwMDY1NiwiZXhwIjoxNTg1ODM2NjU2LCJuYmYiOjE1NTQzMDA2NTYsImp0aSI6IjE1QVU0ZUpQS1kzTXlOQmMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.HfgmVqIQaAQnv6YbwrNizcOJl0AdauU6TESK3mVoV-k','2019-04-03 21:10:56','2020-04-02 21:10:56','UNKNOWN',NULL,1,'2019-04-03 21:10:56','6','2019-04-03 21:17:45',NULL,'2019-04-03 21:17:45',NULL),
(36,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDMwMTA2NSwiZXhwIjoxNTg1ODM3MDY1LCJuYmYiOjE1NTQzMDEwNjUsImp0aSI6IlRqQTE3dGIza1pWOGF6R3UiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.jhQbQn9bBII7wPyR6RFZjryxM_WGPM7p5AlePAE80_w','2019-04-03 21:17:45','2020-04-02 21:17:45','UNKNOWN',NULL,1,'2019-04-03 21:17:45','6','2019-04-03 21:48:19',NULL,'2019-04-03 21:48:19',NULL),
(37,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDMwMjg5OSwiZXhwIjoxNTg1ODM4ODk5LCJuYmYiOjE1NTQzMDI4OTksImp0aSI6ImJET08zaHB0M3R0MjJIV20iLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.hpaasYqlU52fTJ5tM5-0xoqw-tzyoqGGh9dHadZ9pKA','2019-04-03 21:48:19','2020-04-02 21:48:19','UNKNOWN',NULL,1,'2019-04-03 21:48:19','6','2019-04-04 08:20:27',NULL,'2019-04-04 08:20:27',NULL),
(38,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDM0MDgyNywiZXhwIjoxNTg1ODc2ODI3LCJuYmYiOjE1NTQzNDA4MjcsImp0aSI6IkZpVXE0alZCaVc0TXZscFQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.GSkL4zR7_Q7obr1pUUZ8qrEUBfICJ-wedJ1a6a1XCpA','2019-04-04 08:20:27','2020-04-03 08:20:27','UNKNOWN',NULL,1,'2019-04-04 08:20:27','6','2019-04-04 20:40:41',NULL,'2019-04-04 20:40:41',NULL),
(39,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDM4NTI0MSwiZXhwIjoxNTg1OTIxMjQxLCJuYmYiOjE1NTQzODUyNDEsImp0aSI6IlFkYWc1UGpxNjFLQ1djNnciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.SQE7i08mFmgNX56EHe2FYtRcmRt1MP9p7XbDY8wjwk0','2019-04-04 20:40:41','2020-04-03 20:40:41','UNKNOWN',NULL,1,'2019-04-04 20:40:41','6','2019-04-04 22:00:00',NULL,'2019-04-04 22:00:00',NULL),
(40,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDM5MDAwMCwiZXhwIjoxNTg1OTI2MDAwLCJuYmYiOjE1NTQzOTAwMDAsImp0aSI6IlhMbHVicjRqaDRTMFcwb3giLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.V05Uw84OUvb8ju1c_fPSQF4-2X8NSzv7HPNwsmDWOtc','2019-04-04 22:00:00','2020-04-03 22:00:00','UNKNOWN',NULL,1,'2019-04-04 22:00:00','6','2019-04-05 08:19:36',NULL,'2019-04-05 08:19:36',NULL),
(41,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzE3NiwiZXhwIjoxNTg1OTYzMTc2LCJuYmYiOjE1NTQ0MjcxNzYsImp0aSI6ImMwZDZrdUFuRzlLaW93SEQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.IFvPKsZX1_8Vi7vVxRSn5tHq1H5rVF5l9Yzat1d7etM','2019-04-05 08:19:36','2020-04-04 08:19:36','UNKNOWN',NULL,1,'2019-04-05 08:19:36','6','2019-04-05 08:20:00',NULL,'2019-04-05 08:20:00',NULL),
(42,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzIwMCwiZXhwIjoxNTg1OTYzMjAwLCJuYmYiOjE1NTQ0MjcyMDAsImp0aSI6InE2aXBKTmswN1BkWklZSjAiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.3pKxe3c0ts-fws3lMegt1cVoEUTIgiyfeNfvCgw1J6k','2019-04-05 08:20:00','2020-04-04 08:20:00','UNKNOWN',NULL,1,'2019-04-05 08:20:00','6','2019-04-05 08:20:31',NULL,'2019-04-05 08:20:31',NULL),
(43,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzIzMSwiZXhwIjoxNTg1OTYzMjMxLCJuYmYiOjE1NTQ0MjcyMzEsImp0aSI6InBVek9ZeWs5aDQ3a3ZmdmUiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.IgAIpOzL6_mR16Nna3J2wao3BSkpztjS1rqu2xygxMo','2019-04-05 08:20:31','2020-04-04 08:20:31','UNKNOWN',NULL,1,'2019-04-05 08:20:31','6','2019-04-05 08:22:13',NULL,'2019-04-05 08:22:13',NULL),
(44,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzMzMywiZXhwIjoxNTg1OTYzMzMzLCJuYmYiOjE1NTQ0MjczMzMsImp0aSI6Im51VWZjUkNVbkVFeElrc0kiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.POJz4NtcY7LUW7ukHRL6GCpxCjAoHsgG4jhjIdzejmM','2019-04-05 08:22:13','2020-04-04 08:22:13','UNKNOWN',NULL,1,'2019-04-05 08:22:13','6','2019-04-05 08:23:22',NULL,'2019-04-05 08:23:22',NULL),
(45,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzQwMiwiZXhwIjoxNTg1OTYzNDAyLCJuYmYiOjE1NTQ0Mjc0MDIsImp0aSI6Ijh3WDBLNFl0NzVqSmd2YWciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.mFx-UxfBNPMqUs_cULwfPepQHVieWEjAtOCAu2Qsr9A','2019-04-05 08:23:22','2020-04-04 08:23:22','UNKNOWN',NULL,1,'2019-04-05 08:23:22','6','2019-04-05 08:29:14',NULL,'2019-04-05 08:29:14',NULL),
(46,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzc1NCwiZXhwIjoxNTg1OTYzNzU0LCJuYmYiOjE1NTQ0Mjc3NTQsImp0aSI6IlMwWFN5OGpub3NqblUxYXIiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.K5JIU-qQcF55TWZYkmZwOGnnf6VMIAWH4GtdM9dZXxM','2019-04-05 08:29:14','2020-04-04 08:29:14','UNKNOWN',NULL,1,'2019-04-05 08:29:14','6','2019-04-05 08:31:34',NULL,'2019-04-05 08:31:34',NULL),
(47,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzg5NCwiZXhwIjoxNTg1OTYzODk0LCJuYmYiOjE1NTQ0Mjc4OTQsImp0aSI6ImNLUHYzd1dOOFpOUU5maHciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.S2arYBBjrhFAxIpUIKlYo8p-ZzhwD7Q54AYHmFV4Tes','2019-04-05 08:31:34','2020-04-04 08:31:34','UNKNOWN',NULL,1,'2019-04-05 08:31:34','6','2019-04-05 08:33:14',NULL,'2019-04-05 08:33:14',NULL),
(48,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyNzk5NCwiZXhwIjoxNTg1OTYzOTk0LCJuYmYiOjE1NTQ0Mjc5OTQsImp0aSI6ImljaFdHeThhQnBtc1dPeDMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ._q5p-vmHu1JrPenV1hI3-pDghc8EbImy2WHRwvkp0qM','2019-04-05 08:33:14','2020-04-04 08:33:14','UNKNOWN',NULL,1,'2019-04-05 08:33:14','6','2019-04-05 08:34:09',NULL,'2019-04-05 08:34:09',NULL),
(49,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyODA0OSwiZXhwIjoxNTg1OTY0MDQ5LCJuYmYiOjE1NTQ0MjgwNDksImp0aSI6ImYxQmdxbDFCRjZOSXcxa20iLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.0cLVVg6hN9lpYiLVq-NdUOZdZxIz2-LdcQnGYI8eVEw','2019-04-05 08:34:09','2020-04-04 08:34:09','UNKNOWN',NULL,1,'2019-04-05 08:34:09','6','2019-04-05 08:35:49',NULL,'2019-04-05 08:35:49',NULL),
(50,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyODE0OSwiZXhwIjoxNTg1OTY0MTQ5LCJuYmYiOjE1NTQ0MjgxNDksImp0aSI6IkdUVDQxd2tNUndEb2p6QnMiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.GS666vXzcVj5_2ziUOlm0_hvlObMqSHewVFJfC6ifzE','2019-04-05 08:35:49','2020-04-04 08:35:49','UNKNOWN',NULL,1,'2019-04-05 08:35:49','6','2019-04-05 08:45:05',NULL,'2019-04-05 08:45:05',NULL),
(51,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyODcwNSwiZXhwIjoxNTg1OTY0NzA1LCJuYmYiOjE1NTQ0Mjg3MDUsImp0aSI6IllQbkZXdkNTTXpldk1BdTciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Yde6y6ZNFaXKCiK_unu6tXVdY662XiuCsk05rGusF0w','2019-04-05 08:45:05','2020-04-04 08:45:05','UNKNOWN',NULL,1,'2019-04-05 08:45:05','6','2019-04-05 08:45:26',NULL,'2019-04-05 08:45:26',NULL),
(52,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyODcyNiwiZXhwIjoxNTg1OTY0NzI2LCJuYmYiOjE1NTQ0Mjg3MjYsImp0aSI6Ikw0S1FNWmxPRHAwREVpem0iLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.z3-XR4otSJUU-w9kxRxb5rGsMt4Wy5Tqr1e2W_GbohQ','2019-04-05 08:45:26','2020-04-04 08:45:26','UNKNOWN',NULL,1,'2019-04-05 08:45:26','6','2019-04-05 08:50:09',NULL,'2019-04-05 08:50:09',NULL),
(53,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyOTAwOSwiZXhwIjoxNTg1OTY1MDA5LCJuYmYiOjE1NTQ0MjkwMDksImp0aSI6IktDWlF6aGp5c3VCUXFLNUIiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.nBZnXHvQaFIrm-raIStRsBGUOY87MscuXLVw1kn3ulY','2019-04-05 08:50:09','2020-04-04 08:50:09','UNKNOWN',NULL,1,'2019-04-05 08:50:09','6','2019-04-05 09:03:19',NULL,'2019-04-05 09:03:19',NULL),
(54,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQyOTc5OSwiZXhwIjoxNTg1OTY1Nzk5LCJuYmYiOjE1NTQ0Mjk3OTksImp0aSI6ImZTQVhSSU9saXBPVzUxY0siLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.A2Kpu-YJOUWdJOq482yZdpipvrwD6D97h2VgIral6TI','2019-04-05 09:03:19','2020-04-04 09:03:19','UNKNOWN',NULL,1,'2019-04-05 09:03:19','6','2019-04-05 09:15:21',NULL,'2019-04-05 09:15:21',NULL),
(55,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQzMDUyMSwiZXhwIjoxNTg1OTY2NTIxLCJuYmYiOjE1NTQ0MzA1MjEsImp0aSI6Im5ONmhpNmx5ODhsV2hmcGQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.rFxkV7YSYIkQk2UHIS_0Gnnvz4xLzy_K2H3WO73JiMQ','2019-04-05 09:15:21','2020-04-04 09:15:21','UNKNOWN',NULL,1,'2019-04-05 09:15:21','6','2019-04-05 10:15:24',NULL,'2019-04-05 10:15:24',NULL),
(56,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQzNDEyNCwiZXhwIjoxNTg1OTcwMTI0LCJuYmYiOjE1NTQ0MzQxMjQsImp0aSI6ImxQaDYxVnlhTTB0YzZ2MjEiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.UqjmlhD3OJ1p2ERo5s0qERfKBALqpDhNohOBQFZToJA','2019-04-05 10:15:24','2020-04-04 10:15:24','UNKNOWN',NULL,1,'2019-04-05 10:15:24','6','2019-04-05 20:38:06',NULL,'2019-04-05 20:38:06',NULL),
(57,2,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQzNDQ5NSwiZXhwIjoxNTg1OTcwNDk1LCJuYmYiOjE1NTQ0MzQ0OTUsImp0aSI6IjY0MWFpazJUbTJJeGs0ZHYiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.PaoS1pJqqveJrEYDTIYt1O_nz4qDcupPnRRYyxmC6S8','2019-04-05 10:21:35','2020-04-04 10:21:35','UNKNOWN',NULL,1,'2019-04-05 10:21:35','2','2019-04-05 10:22:20',NULL,'2019-04-05 10:22:20',NULL),
(58,2,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQzNDU0MCwiZXhwIjoxNTg1OTcwNTQwLCJuYmYiOjE1NTQ0MzQ1NDAsImp0aSI6IkFBY3RYTXZZc2NzTzd3d2MiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.iUSQl8NKyqJsgn7QDQWbtN03HX4KtSms3jR0MTVCqhk','2019-04-05 10:22:20','2020-04-04 10:22:20','UNKNOWN',NULL,1,'2019-04-05 10:22:20','2','2019-04-05 13:46:05',NULL,'2019-04-05 13:46:05',NULL),
(59,2,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQ0Njc2NSwiZXhwIjoxNTg1OTgyNzY1LCJuYmYiOjE1NTQ0NDY3NjUsImp0aSI6Iklqd29kclBPdGdESFZMY2EiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.8PT938x5C8AlBbIv77HxVBJESXx3Cd8c77Yj-wEGS2U','2019-04-05 13:46:05','2020-04-04 13:46:05','UNKNOWN',NULL,0,'2019-04-05 13:46:05','2',NULL,NULL,NULL,NULL),
(60,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQ3MTQ4NiwiZXhwIjoxNTg2MDA3NDg2LCJuYmYiOjE1NTQ0NzE0ODYsImp0aSI6IjdmODNWSnhqNXhqQXZSRTUiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.yrY29ZnxpMgSEP6TWgiBdhis3qiccOWK8K4lJd1Y-n0','2019-04-05 20:38:06','2020-04-04 20:38:06','UNKNOWN',NULL,1,'2019-04-05 20:38:06','6','2019-04-05 21:30:35',NULL,'2019-04-05 21:30:35',NULL),
(61,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQ3NDYzNSwiZXhwIjoxNTg2MDEwNjM1LCJuYmYiOjE1NTQ0NzQ2MzUsImp0aSI6IklWeGpiU1ZQemZlaENhNXoiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.fyH4ESX3Z0gm1gTj2GLEUHMzJPgrCQH9bJ9pfnYXmYI','2019-04-05 21:30:35','2020-04-04 21:30:35','UNKNOWN',NULL,1,'2019-04-05 21:30:35','6','2019-04-05 23:15:52',NULL,'2019-04-05 23:15:52',NULL),
(62,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDQ4MDk1MiwiZXhwIjoxNTg2MDE2OTUyLCJuYmYiOjE1NTQ0ODA5NTIsImp0aSI6IlBPTUxvbmxUWk5pWGh6ZmUiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.vejzVje4XReI79K3z1t-X1E-IqbLUEgiFIBkctiZJ3s','2019-04-05 23:15:52','2020-04-04 23:15:52','UNKNOWN',NULL,1,'2019-04-05 23:15:52','6','2019-04-06 10:14:23',NULL,'2019-04-06 10:14:23',NULL),
(63,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUyMDQ2MywiZXhwIjoxNTg2MDU2NDYzLCJuYmYiOjE1NTQ1MjA0NjMsImp0aSI6IlpnR2xoQ2t5RjQ1OTNZNGoiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.uAskkuk8ScXzENIyUOFCSKFirTk6Fh3ZbxJJkscm5rY','2019-04-06 10:14:23','2020-04-05 10:14:23','UNKNOWN',NULL,1,'2019-04-06 10:14:23','6','2019-04-06 12:05:44',NULL,'2019-04-06 12:05:44',NULL),
(64,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUyNzE0NCwiZXhwIjoxNTg2MDYzMTQ0LCJuYmYiOjE1NTQ1MjcxNDQsImp0aSI6IkhiaVM2QVVHczRINktXUUciLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.HSvmptlcGoEXT2V0PHotn-kh4mVXc8GK5snPPK_4b7g','2019-04-06 12:05:44','2020-04-05 12:05:44','UNKNOWN',NULL,1,'2019-04-06 12:05:44','6','2019-04-06 12:15:10',NULL,'2019-04-06 12:15:10',NULL),
(65,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUyNzcxMCwiZXhwIjoxNTg2MDYzNzEwLCJuYmYiOjE1NTQ1Mjc3MTAsImp0aSI6Im9UVU9tWTR1WGhYZGV4ZE0iLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.a3nqYLh1CBhQYg6zhq6GJTjjsVm4GfkEpbE-UHttKwI','2019-04-06 12:15:10','2020-04-05 12:15:10','UNKNOWN',NULL,1,'2019-04-06 12:15:10','6','2019-04-06 12:25:33',NULL,'2019-04-06 12:25:33',NULL),
(66,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUyODMzMywiZXhwIjoxNTg2MDY0MzMzLCJuYmYiOjE1NTQ1MjgzMzMsImp0aSI6IjE2OTY1U0Y2ZlZPSzBJYjgiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.A9GGK4hOiGLVBTDEDloXn_igdvuHL1nzRd1n7APtJCE','2019-04-06 12:25:33','2020-04-05 12:25:33','UNKNOWN',NULL,1,'2019-04-06 12:25:33','6','2019-04-06 12:37:17',NULL,'2019-04-06 12:37:17',NULL),
(67,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUyOTAzNywiZXhwIjoxNTg2MDY1MDM3LCJuYmYiOjE1NTQ1MjkwMzcsImp0aSI6InFkSlFNdlV0ek5mVjVmZ0oiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.GkX8mmXHtQpf2Ai7NsxMMx7quyx0g8jN62sg5tsg5r8','2019-04-06 12:37:17','2020-04-05 12:37:17','UNKNOWN',NULL,1,'2019-04-06 12:37:17','6','2019-04-06 14:39:16',NULL,'2019-04-06 14:39:16',NULL),
(68,6,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDUzNjM1NiwiZXhwIjoxNTg2MDcyMzU2LCJuYmYiOjE1NTQ1MzYzNTYsImp0aSI6IlVhczlBT1VjMGl3UWVZcEQiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.i19JY_3CpG3PYh8TXxY4jIcEN3muhrtGoMFh5TcHOUE','2019-04-06 14:39:16','2020-04-05 14:39:16','UNKNOWN',NULL,0,'2019-04-06 14:39:16','6',NULL,NULL,NULL,NULL),
(69,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDU0Njg1NSwiZXhwIjoxNTg2MDgyODU1LCJuYmYiOjE1NTQ1NDY4NTUsImp0aSI6IjZ1TFp5dnNBNkl3Vks0aVUiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.c9egyZLRndWFoTHF0QuYMxc5p35udQ9lWWuISmgDdfk','2019-04-06 17:34:15','2020-04-05 17:34:15','UNKNOWN',NULL,1,'2019-04-06 17:34:15','8','2019-04-06 17:34:37',NULL,'2019-04-06 17:34:37',NULL),
(70,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDU0Njg3NywiZXhwIjoxNTg2MDgyODc3LCJuYmYiOjE1NTQ1NDY4NzcsImp0aSI6ImthS3RtY0JQOTcyWjBhSXIiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.6IEl9tQ-3JwafKIEMlzGVnsQBjkuFfCzjt01nJNwzGA','2019-04-06 17:34:37','2020-04-05 17:34:37','UNKNOWN',NULL,1,'2019-04-06 17:34:37','8','2019-04-06 18:42:55',NULL,'2019-04-06 18:42:55',NULL),
(71,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDU1MDk3NSwiZXhwIjoxNTg2MDg2OTc1LCJuYmYiOjE1NTQ1NTA5NzUsImp0aSI6InJVbTNXTUxpcUtkUnVWMXgiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.jPeKG4WeotJ93eqi_xL0413T1zfBsRB2wM2tqvrvwGM','2019-04-06 18:42:55','2020-04-05 18:42:55','UNKNOWN',NULL,1,'2019-04-06 18:42:55','8','2019-04-06 18:43:21',NULL,'2019-04-06 18:43:21',NULL),
(72,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDU1MTAwMSwiZXhwIjoxNTg2MDg3MDAxLCJuYmYiOjE1NTQ1NTEwMDEsImp0aSI6IkhQa0pGTlBLMEZYeWVaV2kiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.ZHks-zdNANX914DZtMOidcjxPf16O0laiUg0r0hV6ZA','2019-04-06 18:43:21','2020-04-05 18:43:21','UNKNOWN',NULL,1,'2019-04-06 18:43:21','8','2019-04-06 20:54:11',NULL,'2019-04-06 20:54:11',NULL),
(73,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDU1ODg1MSwiZXhwIjoxNTg2MDk0ODUxLCJuYmYiOjE1NTQ1NTg4NTEsImp0aSI6IkpQbzVvQ1JjUWh0c2pCajIiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.4ZJ_4m_CGjvP_tQ-MowClQtdd-jlnqw0LhdE_y1F44s','2019-04-06 20:54:11','2020-04-05 20:54:11','UNKNOWN',NULL,1,'2019-04-06 20:54:11','8','2019-04-07 16:41:47',NULL,'2019-04-07 16:41:47',NULL),
(74,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDYzMDEwNywiZXhwIjoxNTg2MTY2MTA3LCJuYmYiOjE1NTQ2MzAxMDcsImp0aSI6IjJnT0Z4NGM5NjY2aElSVUwiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.xw3tNgPO7pdWuzuz09bkz1BEcd_--f2yIqZ6iYpwODA','2019-04-07 16:41:47','2020-04-06 16:41:47','UNKNOWN',NULL,1,'2019-04-07 16:41:47','8','2019-04-07 16:42:02',NULL,'2019-04-07 16:42:02',NULL),
(75,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDYzMDEyMiwiZXhwIjoxNTg2MTY2MTIyLCJuYmYiOjE1NTQ2MzAxMjIsImp0aSI6IlRRRXZjZVR3NUh2SnRuaU8iLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.IVjwYnXWYggXmH3-2kY8Q-7t4hrajjzItEzd0ueyFHc','2019-04-07 16:42:02','2020-04-06 16:42:02','UNKNOWN',NULL,1,'2019-04-07 16:42:02','8','2019-04-07 16:43:04',NULL,'2019-04-07 16:43:04',NULL),
(76,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDYzMDE4NCwiZXhwIjoxNTg2MTY2MTg0LCJuYmYiOjE1NTQ2MzAxODQsImp0aSI6IkZpWFhrcDVVYjcwb2h5NXIiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.bTD_Pz1bj7-R2x93Gg8euj4p4p05ZYC9nYyFCs_eY9Y','2019-04-07 16:43:04','2020-04-06 16:43:04','UNKNOWN',NULL,1,'2019-04-07 16:43:04','8','2019-04-07 16:45:36',NULL,'2019-04-07 16:45:36',NULL),
(77,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmstb2ZmaWNlLmtwaXMudm4vYXV0aC9sb2dpbiIsImlhdCI6MTU1NDYzMDMzNiwiZXhwIjoxNTg2MTY2MzM2LCJuYmYiOjE1NTQ2MzAzMzYsImp0aSI6Imdlb1pTT21FRWpNRTBlTksiLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.yeTglv4A_EJY_lTNPimnGQc-SWYlZDSt-bMXs5p-EYI','2019-04-07 16:45:36','2020-04-06 16:45:36','UNKNOWN',NULL,1,'2019-04-07 16:45:36','8','2019-04-21 14:12:17',NULL,'2019-04-21 14:12:17',NULL),
(78,7,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vay1vZmZpY2UubG9jYWwvYXV0aC9sb2dpbiIsImlhdCI6MTU1NDY5NjAwNiwiZXhwIjoxNTg2MjMyMDA2LCJuYmYiOjE1NTQ2OTYwMDYsImp0aSI6InRxVmpUSEQxQzlMc2t2VmYiLCJzdWIiOjcsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.OPwOWeyuoxpILzFf0QDBPnO0GPXjYKUGWZwTSai1erg','2019-04-08 11:00:06','2020-04-07 11:00:06','UNKNOWN',NULL,1,'2019-04-08 11:00:06','7','2019-04-09 22:31:17',NULL,'2019-04-09 22:31:17',NULL),
(79,7,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vay1vZmZpY2UubG9jYWwvYXV0aC9sb2dpbiIsImlhdCI6MTU1NDgyMzg3NywiZXhwIjoxNTg2MzU5ODc3LCJuYmYiOjE1NTQ4MjM4NzcsImp0aSI6IlN1Y2NUNGU2aXZ6U2dvY2MiLCJzdWIiOjcsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.PwZuPXLUMtDFQGlv95Jd8pDIhYU5v_xQt4I6AJflma4','2019-04-09 22:31:17','2020-04-08 22:31:17','UNKNOWN',NULL,1,'2019-04-09 22:31:17','7','2019-04-17 21:00:57',NULL,'2019-04-17 21:00:57',NULL),
(80,7,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vay1vZmZpY2UubG9jYWwvYXV0aC9sb2dpbiIsImlhdCI6MTU1NTUwOTY1NiwiZXhwIjoxNTg3MDQ1NjU3LCJuYmYiOjE1NTU1MDk2NTcsImp0aSI6IjVPSDBGb05OS0lhMGtCYzEiLCJzdWIiOjcsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Ulq5S1OM4Rufp_sGbFJqKa3BJdJyC2N4Xk8gXxs6IbU','2019-04-17 21:00:57','2020-04-16 21:00:57','UNKNOWN',NULL,0,'2019-04-17 21:00:57','7',NULL,NULL,NULL,NULL),
(81,8,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vay1vZmZpY2UubG9jYWwvYXV0aC9sb2dpbiIsImlhdCI6MTU1NTgzMDczNywiZXhwIjoxNTg3MzY2NzM3LCJuYmYiOjE1NTU4MzA3MzcsImp0aSI6ImtXRUN2SHlUT3JEMWdudW0iLCJzdWIiOjgsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.8kdRl-SDSh_Xf7T7MyOTYPQ4Jmima5ocu82Hj9_NpGk','2019-04-21 14:12:17','2020-04-20 14:12:17','UNKNOWN',NULL,0,'2019-04-21 14:12:17','8',NULL,NULL,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(72) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verify_code` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_code` datetime DEFAULT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `role_id` bigint(20) unsigned DEFAULT '4',
  `note` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_users_roleId_roles_id` (`role_id`),
  KEY `FK_users_departments` (`department_id`),
  CONSTRAINT `FK_users_departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_users_roleId_roles_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`code`,`username`,`email`,`password`,`phone`,`verify_code`,`expired_code`,`department_id`,`role_id`,`note`,`is_active`,`is_super`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`,`price_show`) values 
(2,'tien.nguyen','tien.nguyen','tien@kpis.com.vn','$2y$10$5YY4QaEa3.jLaUtqKewqzOlVEMfSpzrrgNLqq/AXpPSzP57v2YFmq','969033276','571387','2019-04-08 10:25:11',NULL,1,NULL,1,1,0,'2017-09-23 12:00:00','1','2019-04-08 10:20:11','1',NULL,NULL,1),
(5,'1546572838','1546572838','ryanthanhtien@gmail.com','$2y$10$KiRpWkuoB3VJ0S/kf0FtcOYUT7GBJ4hKkbisgOqybNAVEJ5RCTQF2','0367560394','276684','2019-04-10 09:27:22',1,1,NULL,1,0,0,'2019-01-04 10:33:58',NULL,'2019-04-10 09:22:22',NULL,NULL,NULL,1),
(6,'quan.pham','quan.pham','huequan98@gmail.com','$2y$10$J7lBroPEmG/Hdo9bUKvDL.Ore4AVULSeI9uxYD1AokmKACSP3ZQe6','123456789','667002','2019-04-08 09:59:03',1,1,NULL,1,0,0,'2019-01-05 13:54:43',NULL,'2019-04-08 09:54:03',NULL,NULL,NULL,1),
(7,'dat.tran','dat.tran','trantiendat041198@gmail.com','$2y$10$w0UJgCSWyAmVgx/ADUmyg.QZa7JSM/NyJbtVatCklW9/nuW5k3VG6','123544213','753282','2019-02-20 09:19:29',NULL,1,NULL,1,0,0,'2019-01-05 13:55:33',NULL,'2019-02-20 09:14:29',NULL,NULL,NULL,1),
(8,'Sang','Sang','sangnguyen0909ds@gmail.com','$2y$10$1hJGMI8cuXLGO.HHfely8.Hic7X2fRYRuLr/NAQoKXjrYAnSw524W','12381274','407761','2019-03-21 12:07:26',NULL,1,NULL,1,1,0,'2019-01-06 10:15:48',NULL,'2019-03-21 12:02:26',NULL,NULL,NULL,1),
(9,'hieu.phan','hieu.phan','hieu.phan@kpis.com.vn','$2y$10$UMYPdq1qLQtT610kR9I6Xu6BLFzHr03y/OEoUdpsBL3/scrzKxMIe','0966944164','336139','2019-03-22 12:43:49',NULL,1,NULL,1,0,0,'2019-01-06 10:16:23',NULL,'2019-03-22 12:38:49',NULL,NULL,NULL,1),
(54,'hieu.phan1','hieu.phan1','tungngaysuynghi96@gmail.com','$2y$10$l0sePUi4IiZXZC44KWqiTOkziuNiuqWtrmqPKtAIl98quWIqmW33S','0966944164','220152','2019-03-08 09:30:04',NULL,4,NULL,1,0,0,'2019-02-20 10:05:11',NULL,'2019-03-08 09:25:04',NULL,NULL,NULL,1),
(61,'HST00288','HST00288','ryanah@gmail.com','$2y$10$.YRMdZJLYmV1C55ifwYSvuKy1iUk0vVUkA02TKfd55jtHGfWhXEjW','9999999','317863','2019-03-20 19:43:02',NULL,4,NULL,1,0,0,'2019-03-20 19:38:02',NULL,'2019-03-20 19:38:02',NULL,NULL,NULL,1),
(63,'sang1','sang1','sang.nguyen09ds@gmail.com','$2y$10$1hJGMI8cuXLGO.HHfely8.Hic7X2fRYRuLr/NAQoKXjrYAnSw524W','394679933','610261','2019-03-21 15:03:26',NULL,1,NULL,1,0,0,'2019-03-21 14:58:27',NULL,'2019-03-21 14:58:27',NULL,NULL,NULL,1),
(64,'abcdf','abcdf','vanquan@gmail.com','$2y$10$h7VfYFCAG/PbEru4uHLUaepWQddVelfEnXxrnUdvWUdBMdYfnE9M2','9090909','662881','2019-03-22 11:27:26',NULL,1,NULL,1,0,0,'2019-03-22 11:22:26',NULL,'2019-03-22 11:22:26',NULL,NULL,NULL,1),
(65,'vanquan','vanquan','admin@gmail.com','$2y$10$qV0T0Eg8OvUuv2vQaocqeO0gCNXpty6y2pP18.OKPFW7e5.26cEfq','969033222','942673','2019-03-22 11:36:34',NULL,1,NULL,1,0,0,'2019-03-22 11:31:34',NULL,'2019-03-22 11:31:34',NULL,NULL,NULL,1),
(68,'sang1223','sang1223','sang1234@gmail.com','$2y$10$o6g1LBYilUL.sz8bOBOYPu6Uxo/q7utT6jNUpb.QAq5F9q2hNzRtq','23456','726767','2019-03-31 15:46:29',NULL,1,NULL,1,0,0,'2019-03-31 15:39:02',NULL,'2019-03-31 15:41:29',NULL,NULL,NULL,1),
(69,'code','code','quan98@gmail.com','$2y$10$T0sj6o2dQjjz6HOH1KHx4.1/xJfTDS5fqYF3jE0GCygom1/.eQTE2','969033276','448976','2019-04-04 21:08:38',NULL,1,NULL,1,0,0,'2019-04-04 21:03:38',NULL,'2019-04-04 21:03:38',NULL,NULL,NULL,1),
(70,'code98','code98','q@gmail.com','$2y$10$nDxk971MAFh.Q/rUIMmIfeCSje0nWNviFX9K74XCpgIDkfMNkA5w.','969033276','493685','2019-04-08 10:23:07',NULL,1,NULL,1,0,0,'2019-04-08 10:06:13',NULL,'2019-04-08 10:18:07',NULL,NULL,NULL,1),
(71,'tessts','tessts','s@gmail.com','$2y$10$cvkzb1l.z0IZIb8Zu8XLVO1Zl23xvIG2oceXuNRzNV.Y3hwM9K9CC','969033276','703435','2019-04-08 10:24:17',NULL,1,NULL,1,0,0,'2019-04-08 10:19:17',NULL,'2019-04-08 10:19:17',NULL,NULL,NULL,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
