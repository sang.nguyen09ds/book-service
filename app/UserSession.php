<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 13/06/2021
 */

namespace App;

/**
 * Class UserSession
 *
 * @package App
 */
class UserSession extends BaseModel {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_sessions';

}
