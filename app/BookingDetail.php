<?php

/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 13/06/2021
 */
namespace App;

class BookingDetail extends BaseModel
{
    protected $table = 'booking_detail';

    protected $fillable = [
        'book_id',
        'booking_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];

}
