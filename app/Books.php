<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 13/06/2021
 */

namespace App;

use Illuminate\Support\Arr;

class Books extends BaseModel
{
    protected $table = 'books';

    protected $fillable = [
        'code',
        'name',
        'url',
        'note',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    public function search($input, $limit)
    {
        $query = Books::select('*')
            ->distinct()
            ->orderBy('id', 'DESC');
        return $query->paginate($limit);
    }
    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        $file_name = '';
        if ($id) {
            $books = Books::find($id);
            if (empty($size)) {
                throw new \Exception('data not found');
            }
            $books->name = Arr::get($input, 'name', $size->name);
            $books->url = $file_name || null;
            $books->note = Arr::get($input, 'note', $size->note);
            $books->updated_at = date("Y-m-d H:i:s", time());
            $size->save();
        } else {
            $param = [
                'code' => Arr::get($input, 'code', NULL),
                'name' => Arr::get($input, 'name', NULL),
                'url' => $file_name || null,
                'note' => Arr::get($input, 'note', NULL),
                'created_at' => date("Y-m-d H:i:s", time()),
                'created_by' => 2,
                'updated_at' => date("Y-m-d H:i:s", time()),
                'updated_by' => 4,
            ];
            $size = $this->create($param);
        }
        return $size;
    }

}
