<?php
/**
 * User: Administrator
 * Date: 28/09/2018
 * Time: 09:35 PM
 */

namespace App\V1\CMS\Validators;


use App\Http\Validators\ValidatorBase;
use App\Supports\Message;
use App\User;
use Illuminate\Http\Request;

class UserCreateValidator extends ValidatorBase
{
    protected function rules()
    {
        return [
            'id'            => 'exists:users,id,deleted_at,NULL',
            'email'         => [
                'nullable',
                function ($attribute, $value, $fail) {
                    $item = User::where('email', $value)->whereNull('deleted_at')->first();
                    if (!empty($item)) {
                        return $fail(Message::get("unique", "$attribute: #$value"));
                    }
                }
            ],
            'code'          => [
                'required',
                'max:50',
                function ($attribute, $value, $fail) {
                    $item = User::where('code', $value)->whereNull('deleted_at')->first();
                    if (!empty($item)) {
                        return $fail(Message::get("unique", "$attribute: #$value"));
                    }
                }
            ],
            'department_id' => 'required|exists:departments,id,deleted_at,NULL',
            'phone'         => 'max:12',
            'password'      => 'required',
            'full_name'     => 'required',
        ];
    }

    protected function attributes()
    {
        return [
            'phone'         => Message::get("phone"),
            'department_id' => Message::get("department_id"),
            'email'         => Message::get("email"),
            'code'          => Message::get("code"),
            'full_name'     => Message::get("full_name"),
            'password'      => Message::get("password"),
        ];
    }
}
