<?php
/**
 * User: SangNguyen
 * Date: 13/06/2021
 * Time: 23:43
 */

namespace App\V1\CMS\Models;
use Illuminate\Support\Facades\DB;

/**
 * Class AbstractModel
 *
 * @package App\V1\CMS\Models
 */
abstract class AbstractModel
{
    protected $model;
    protected $modelName;

    public function __construct($model = null)
    {
        $modelName = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        $this->modelName = str_replace('Model', '', $modelName);
        $modelName = "\\App\\" . $this->modelName;

        $this->model = ($model) ?: new $modelName();
    }

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function create(array $data)
    {
        // Create the model
        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }

        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function update(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getRelatedTable()
    {
        $relatedTables = DB::table('INFORMATION_SCHEMA.KEY_COLUMN_USAGE')
            ->select(['REFERENCED_TABLE_NAME', 'TABLE_NAME', 'COLUMN_NAME'])
            ->whereRaw('TABLE_SCHEMA = SCHEMA()')
            ->where('REFERENCED_TABLE_NAME', $this->getTable())
            ->orderBy('REFERENCED_TABLE_NAME')
            ->get()
            ->toArray();

        return $relatedTables;
    }
}
