<?php
/**
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 10:11 AM
 */

namespace App\V1\CMS\Models;


use App\BOOK;
use App\Booking;
use App\BookingDetail;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class BookingModel
 * @package App\V1\CMS\Models
 */
class BookingModel extends AbstractModel
{
    public function __construct(Booking $model = null)
    {
        parent::__construct($model);
    }

    public function search($input, $limit)
    {
        $query = Booking::select('*')
            ->distinct()
            ->orderBy('id', 'DESC');
        return $query->paginate($limit);
    }

    public function searchBookingUser()
    {
        //expired_at
        $idUser = BOOK::getCurrentUserId();
        $query = DB::table('books')
            ->select('books.id as id', 'books.name as name_books', 'books.url as url_books', 'booking.expired_at as expired_at')
            ->leftJoin('booking_detail', 'booking_detail.book_id', '=', 'books.id')
            ->leftJoin('booking', 'booking.id', '=', 'booking_detail.booking_id')
            ->where('booking.user_id', $idUser)->get();
        return $query;
    }

    public function upsert($input)
    {
        $id = !empty($input['id']) ? $input['id'] : 0;
        if ($id) {
            $booking = Booking::find($id);
            if (empty($booking)) {
                throw new \Exception('data not found');
            }
            $booking->name = Arr::get($input, 'name', $booking->name);
            $booking->description = Arr::get($input, 'description', $booking->title);
            $booking->is_active = Arr::get($input, 'is_active', $booking->is_active);
            $booking->updated_at = date("Y-m-d H:i:s", time());
            $booking->updated_by = BOOK::getCurrentUserId();
            $booking->save();
        } else {
            $param = [
                'user_id' => BOOK::getCurrentUserId(),
                'description' => Arr::get($input, 'description', NULL),
                'expired_at' => Arr::get($input, 'expired_at', NULL),
            ];
            $booking = $this->create($param);
            foreach ($input['booking'] as $detail) {

                $bookingDetail = new BookingDetail();
                $bookingDetail->create([
                    'booking_id' => $booking->id,
                    'book_id' => $detail['book_id'],
                    'created_at' => date("Y-m-d H:i:s", time()),
                    'created_by' => BOOK::getCurrentUserId(),
                    'is_active' => 1,
                    'updated_at' => date("Y-m-d H:i:s", time()),
                    'updated_by' => BOOK::getCurrentUserId(),
                ]);
            }
        }
        return $booking;
    }
}