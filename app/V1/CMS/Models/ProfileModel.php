<?php
/**
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 10:11 AM
 */

namespace App\V1\CMS\Models;


use App\Profile;

class ProfileModel extends AbstractModel
{
    /**
     * CityModel constructor.
     * @param Profile|null $model
     */
    public function __construct(Profile $model = null)
    {
        parent::__construct($model);
    }
}