<?php
/**
 * User: SangNguyen
 * Date: 13/06/2021
 * Time: 09:32 PM
 */

namespace App\V1\CMS\Models;

use App\User;

class UserModel extends AbstractModel
{
    public function __construct(User $model = null)
    {
        parent::__construct($model);
    }

    public
    function search($input = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        if (!empty($input['code'])) {
            $query = $query->where('code', 'like', "%{$input['code']}%");
        }
        if (!empty($input['phone'])) {
            $query = $query->where('phone', 'like', "%{$input['phone']}%");
        }
        if (!empty($input['full_name'])) {
            $query = $query->whereHas('profile', function ($q) use ($input) {
                $q->where('full_name', 'like', "%{$input['full_name']}%");
            });
        }
        if (isset($input['is_active'])) {
            $query = $query->where('is_active', 'like', "%{$input['is_active']}%");
        }

        if ($limit) {
            if ($limit === 1) {
                return $query->first();
            } else {
                return $query->paginate($limit);
            }
        } else {
            return $query->get();
        }
    }
}
