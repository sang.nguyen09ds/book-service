<?php
/**
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 10:11 AM
 */

namespace App\V1\CMS\Models;


use App\Books;

/**
 * Class BookingModel
 * @package App\V1\CMS\Models
 */
class BooksModel extends AbstractModel
{
    public function __construct(Books $model = null)
    {
        parent::__construct($model);
    }
}