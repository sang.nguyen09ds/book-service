<?php

namespace App\V1\CMS\Transformers\Booking;
use App\Booking;

use League\Fractal\TransformerAbstract;

class BookingUserTransformer extends TransformerAbstract
{
    public function transform(Booking $booking)
    {
        //  ->select('books.id as id', 'books.name as name_books', 'books.url as url_books')
        try {
            return [
                'id'              => $booking->id,
                'name'            => $booking->name_books,
                'url'           => $booking->url_books,
                'expired_at'           => $booking->expired_at,
            ];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}