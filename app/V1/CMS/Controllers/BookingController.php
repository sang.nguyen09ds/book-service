<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 10/21/2019
 * Time: 11:43 PM
 */

namespace App\V1\CMS\Controllers;

use App\BOOK;
use App\V1\CMS\Models\BookingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\V1\CMS\Transformers\Booking\BookingUserTransformer;

class BookingController extends BaseController
{
    /**
     * @var BookingModel
     */
    protected $model;

    /**
     * SizeController constructor.
     */
    public function __construct()
    {
        $this->model = new BookingModel();
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 20);
        $result = $this->model->search($input = [], $limit);
        return response()->json($result, 200);
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $rules = [
            'booking' => ["required", "array", "min:1", "max:5"], // validate an array contains minimum 2 elements and maximum 5
            'expired_at' => 'required|date_format:Y-m-d H:i:s',
            'booking.*.book_id' => 'exists:books,id',
        ];
        $allBookingDetail = Arr::pluck($input['booking'], 'book_id');

        $checkDateExit = DB::table('booking_detail')
            ->leftJoin('booking', 'booking.id', '=', 'booking_detail.booking_id')
            ->whereIn('booking_detail.book_id', $allBookingDetail)
            ->where('booking.user_id', BOOK::getCurrentUserId())
            ->select('booking_detail.*')->count();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'message' => $errors,
                'status' => 400,
            ], 422);
        }
        if ($checkDateExit !== 0) {
            return response()->json([
                'message' => 'Borrowed books already exist in the system',
                'status' => 400,
            ], 422);
        }
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->response->errorBadRequest($ex->getMessage());
        }
        return response()->json($result, 200);
    }

    /**
     * @param BookingUserTransformer $bookingUserTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function searchBookingByUser(BookingUserTransformer $bookingUserTransformer)
    {
        $result = $this->model->searchBookingUser();
        return $this->response->item($result, $bookingUserTransformer);

    }
}
