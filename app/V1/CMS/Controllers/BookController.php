<?php


namespace App\V1\CMS\Controllers;

use App\Books;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BookController extends BaseController
{

    protected $model;

    public function __construct()
    {
        $this->model = new  Books();
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $limit = Arr::get($input, 'limit', 20);
        $result = $this->model->search($input = [], $limit);
        return response()->json($result, 200);
    }

    public function create(Request $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();

            return $this->response->errorBadRequest('error');
        }
        return response()->json($result, 200);
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            $result = $this->model->upsert($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->response->errorBadRequest('error');
        }
        return response()->json($result, 200);
    }
}
