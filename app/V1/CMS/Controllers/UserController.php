<?php

namespace App\V1\CMS\Controllers;
use App\V1\CMS\Models\UserModel;
use App\V1\CMS\Transformers\User\UserTransformer;
use Illuminate\Http\Request;


/**
 * Class UserController
 *
 * @package App\V1\CMS\Controllers
 */
class UserController extends BaseController
{

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * @param UserTransformer $userTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function search(Request $request, UserTransformer $userTransformer)
    {
        $input = $request->all();
        $limit = array_get($input, 'limit', 3);
        $result = $this->model->search($input, [], $limit);
        return $this->response->paginator($result, $userTransformer);
    }
}
