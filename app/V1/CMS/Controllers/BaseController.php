<?php
/**
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 14:18
 */

namespace App\V1\CMS\Controllers;
use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;

class BaseController extends Controller
{
    use Helpers;
}