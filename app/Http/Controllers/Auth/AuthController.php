<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 13/06/2021
 */

namespace App\Http\Controllers\Auth;
use App\Http\Validators\Admin\CMSLoginValidator;
use App\Http\Controllers\Controller;
use App\Http\Validators\Admin\RegisterValidator;
use App\Http\Validators\Admin\LoginValidator;
use App\Supports\Message;
use App\User;
use App\UserSession;
use App\V1\CMS\Models\ProfileModel;
use App\V1\CMS\Models\UserModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{
    protected static $_user_type_user;
    protected static $_user_expired_day;
    /**
     *
     */
    protected $jwt;

    protected $model;

    /**
     * AuthController constructor.
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
        self::$_user_type_user = "USER";
        self::$_user_expired_day = 365;
        $this->model = new UserModel();
    }

    /**
     * @param Request $request
     * @param LoginValidator $loginValidator
     *
     * @return mixed
     * @throws \Exception
     */
    public function authenticate(Request $request, CMSLoginValidator $loginValidator)
    {
        $input = $request->all();
       $param['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
        $loginValidator->validate($input);
        $credentials = $request->only('username', 'password');
        try {
            $token = $this->jwt->attempt($credentials);

            if (!$token) {
                return response()->json(['errors' => [[Message::get("users.admin-login-invalid")]]], 401);
            }

            $user = User::where(['username' => $input['username']])->first();

            if (empty($user)) {
                return response()->json(['errors' => [[Message::get("users.admin-login-invalid")]]], 401);
            }

            if ($user->is_active == "0") {
                return response()->json(['errors' => [[Message::get("users.user-inactive")]]], 401);
            }
            // Write User Session
            $now = time();
            UserSession::where('user_id', $user->id)->update([
                'deleted'    => 1,
                'updated_at' => date("Y-m-d H:i:s", $now),
                'updated_by' => $user->user_id,
            ]);
            UserSession::where('user_id', $user->id)->delete();

            $device_type = array_get($input, 'device_type', 'UNKNOWN');
            UserSession::insert([
                'user_id'     => $user->id,
                'token'       => $token,
                'login_at'    => date("Y-m-d H:i:s", $now),
                'expired_at'  => date("Y-m-d H:i:s", ($now + config('jwt.ttl') * 60)),
                'device_type' => $device_type,
                'device_id'   => array_get($input, 'device_id'),
                'deleted'     => 0,
                'created_at'  => date("Y-m-d H:i:s", $now),
                'created_by'  => $user->id,
            ]);
        } catch (JWTException $e) {

            return response()->json(['errors' => [[$e->getMessage()]]], 500);
        } catch (\Exception $ex) {
            return response()->json(['errors' => [[$ex->getMessage()]]], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * @param Request $request
     * @param RegisterValidator $registerValidator
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userRegister(Request $request, RegisterValidator $registerValidator)
    {
        $input = $request->all();

        $registerValidator->validate($input);

        if ($input['phone'] < 0 || strlen($input['phone']) < 9 || strlen($input['phone']) > 14) {
            return response()->json(['errors' => [[Message::get("V002", Message::get("phone"))]]], 500);
        }
        try {
            $phone = str_replace(" ", "", $input['phone']);
            $phone = preg_replace('/\D/', '', $phone);
            $param = [
                'username'  => $phone,
                'phone'     => $phone,
                'code'      => $phone,
                'email'      => !empty($input['email']) ? $input['email'] : null,
                'is_active' => 1,
            ];

            if (!empty($input['password'])) {
                $param['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
            }

            DB::beginTransaction();
            $user = $this->model->create($param);

            $names = explode(" ", trim($input['name']));
            $first = $names[0];
            unset($names[0]);
            $last = !empty($names) ? implode(" ", $names) : null;

            $prProfile = [
                'is_active'  => 1,
                'first_name' => $first,
                'last_name'  => $last,
                'short_name' => $input['name'],
                'full_name'  => $input['name'],
                'genre'      => array_get($input, 'genre', 'O'),
                'address'    => array_get($input, 'address', null),
                'phone'      => $input['phone'],
                'user_id'    => $user->id,
            ];

            // Create Profile
            $profileModel = new ProfileModel();
            $profileModel->create($prProfile);

            DB::commit();

            return response()->json(['status' => Message::get("users.register-success", $input['phone'])], 200);
        } catch (QueryException $ex) {
            return response()->json(['errors' => [$ex->getMessage()]], 401);
        } catch (\Exception $ex) {
            return response()->json(['errors' => [$ex->getMessage()]], 401);
        }
    }
}
