<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 27/12/2018
 * Time: 10:17 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class AuthAdmin
{
    public function handle($request, Closure $next)
    {
      Config::set('jwt.user' , 'App\User');
      Config::set('auth.providers.users.model', \App\User::class);

        return $next($request);
    }
}