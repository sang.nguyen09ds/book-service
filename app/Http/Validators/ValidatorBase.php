<?php

namespace App\Http\Validators;

use App\Supports\Message;
use Dingo\Api\Exception\ValidationHttpException;
use Validator;

abstract class ValidatorBase
{
    abstract protected function rules();

    public function validate($input)
    {
        $validator = Validator::make($input, $this->rules(), $this->messages());
        $validator->setAttributeNames($this->attributes());

        if ($validator->fails()) {
            throw new ValidationHttpException ($validator->errors()->getMessages());
        }

        return $validator;
    }

    protected function messages()
    {
        $attributes = array_keys(config('validation'));
        $output = [];
        foreach ($attributes as $attribute) {
            $output["*.$attribute"] = Message::get($attribute, ':attribute');
        }
        return $output;
    }
}