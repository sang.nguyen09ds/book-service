<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 13/06/2021
 */
namespace App;
use Illuminate\Support\Arr;

class Booking extends BaseModel
{
    protected $table = 'booking';

    protected $fillable = [
        'user_id',
        'description',
        'expired_at',
        'deleted',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];
}
