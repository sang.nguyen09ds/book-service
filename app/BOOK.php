<?php

namespace App;

use App\Supports\Message;
use Dingo\Api\Http\Response;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class BOOK
{
    private static $_data;

    public static final function info()
    {
        self::__getUserInfo();

        return self::$_data;
    }

    private static final function __getUserInfo()
    {

        $token = JWTAuth::getToken();

        if (!$token) {
            return response()->json([
                'message' => 'A token is required',
                'status_code' => Response::HTTP_UNAUTHORIZED,
            ], Response::HTTP_UNAUTHORIZED);
        }
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if (empty($user)) {
                return response()->json([
                    'message' => Message::get("V003", Message::get('customers')),
                    'status_code' => Response::HTTP_UNAUTHORIZED,
                ], Response::HTTP_UNAUTHORIZED);
            }
        } catch (TokenExpiredException $ex) {
            return response()->json([
                'message' => $ex->getMessage(),
                'status_code' => Response::HTTP_UNAUTHORIZED,
            ], Response::HTTP_UNAUTHORIZED);
        } catch (TokenBlacklistedException $blacklistedException) {
            return response()->json([
                'message' => $blacklistedException,
                'status_code' => Response::HTTP_UNAUTHORIZED,
            ], Response::HTTP_UNAUTHORIZED);
        }
        self::$_data = [
            'id' => $user->id,
            'email' => $user->email,
            'user' => $user->user,
            'first_name' => object_get($user, 'profile.first_name', null),
            'last_name' => object_get($user, 'profile.last_name', null),
            'full_name' => object_get($user, 'profile.full_name', null),
            'is_super' => $user->is_super,
        ];
    }

    //------------------static function-------------------------------

    public static final function isSuperUser()
    {
        $userInfo = self::info();

        return $userInfo['is_super'] == 1 ? true : false;
    }

    public static final function getCurrentUserId()
    {
        $userInfo = self::info();

        return $userInfo['id'];
    }
}
