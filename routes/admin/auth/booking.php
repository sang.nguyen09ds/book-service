<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 11:41 PM
 */

/**
 * create item booking
 */

$api->get('/booking-user', [
    'uses' => 'BookingController@searchBookingByUser',
]);

$api->get('/booking', [
    'uses' => 'BookingController@search',
]);
$api->post('/booking', [
    'uses' => 'BookingController@create',
]);
$api->put('/booking/{id:[0-9]+}', [
    'uses' => 'BookingController@update',
]);

