<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Authorized Group
$api->version('v1', ['middleware' => ['cors2', 'verifySecret',]
], function ($api) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\V1\CMS\Controllers'], function ($api) {

        $api->options('/{any:.*}', function () {
            return response(['status' => 'success'])
                ->header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                ->header('Access-Control-Allow-Headers', 'Authorization, Content-Type, Origin');
        });

        $api->get('/', function () {
            return ['api-status' => 'CMS API Ok!'];
        });
        $api->get('/user', 'UserController@search');

        // Books
        require __DIR__ . '/booking.php';
        // Users
        // Books
        require __DIR__ . '/book.php';
        // Users
        require __DIR__ . '/user.php';

    });
});

