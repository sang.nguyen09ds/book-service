<?php
/**
 * Created by PhpStorm.
 * User: SangNguyen
 * Date: 14/06/2021
 * Time: 11:41 PM
 */
/**
 * create item books
 */
$api->get('/books', 'BookController@search');
$api->post('/books', 'BookController@create');

